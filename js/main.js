const slide = document.getElementsByClassName("image-to-show");
let currentSlide = 0;
let slideInterval = setInterval(nextSlide,3000);

function nextSlide() {
    slide[currentSlide].classList.remove("active");
    currentSlide = (currentSlide+1)%slide.length;
    slide[currentSlide].classList.add("active");
}
const btnStop = document.getElementById("stop")
const btnResume = document.getElementById("resume");
console.log(document.children[0].children[1].children);

btnStop.addEventListener("click",function(){
  clearInterval(slideInterval);
  btnResume.disabled = false;
  btnStop.disabled = true;
});
btnResume.addEventListener("click",function(){
  slideInterval = setInterval(nextSlide,3000);
  btnResume.disabled = true;
  btnStop.disabled = false;
});